<head>
	<!-- Stylesheets -->
	<link rel="stylesheet" type="text/css" href="style.css"/></link>
		<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.4.1/jquery.min.js"></script>
		<script type="text/javascript" src="custom.js"></script>
		
		<!-- for group buttons recent popular and create in content header -->
		<meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
  
  <!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">

<!-- Optional theme -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap-theme.min.css">

<!-- Latest compiled and minified JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
  
  
  

</head>
<body>
	<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.4.1/jquery.min.js"></script>
	<script type="text/javascript" src="custom.js"></script>

 <!-- Includes funcitons which help in navigating -->
	<?php include('Session.php');?>	
<div id = "header">
	<div class="container">
		<div class="btn-group btn-group-justified">
  			<div class="btn-group">
    			<input type="button" class="btn btn-link" value="Logo"  onclick="return loadPage('Main')">
			</div>
			<div class="btn-group">
				<input type="text" class="form-control" placeholder="Search" name="srch-term" id="srch-term">
			</div>
			<div class="btn-group">
    			<input type="button"  class="btn btn-link" value="Gifs"  onclick="return loadPage('GifCollections')">
			</div>
			<div class="btn-group">
				<input type="button"  class="btn btn-link" value="Memes"  onclick="return loadPage('MemeCollections')">
			</div>
			<div class="btn-group">
				<button type="button"  class="btn btn-link" data-toggle="modal" data-target="#New">New</button>
			</div>
			<div class="btn-group">
				<button type="button"  class="btn btn-link" data-toggle="modal" data-target="#Login">Login/Settings</button>
			</div>
		</div>
	</div>
</div>
  <!-- New -->
  <div class="modal fade" id="New" role="dialog">
<div class="modal-dialog">
		<div class="modal-content" id="modal">
			<h4 class="modal-title">New Post : </h4>
			<div class="modal-body" >
			   <label for="name" class="col-sm-2 control-label">Title :</label>
			   <br>
					<div class="col-sm-16">
						<input type="text" class="form-control" id="contactText" name="name" placeholder="Title of post" value="">
					</div>
			   <label for="name" class="col-sm-6 control-label">Upload Gifs/Memes:</label>
			   <div class="col-sm-16">
						<input type="text" class="form-control" id="contactText" name="name" placeholder="Browse file " value="">
					</div>
				<label for="name" class="col-sm-6 control-label">Add Text :</label>
    				<div class="form-row">
      					<div id="comment-message" class="form-row">
						<textarea name = "comment" placeholder = "Sub-Text..." id = "comment" ></textarea>
      					<br><br>
      					<button type="button" id="close-submit" class="btn btn-default pull-left" data-dismiss="modal">Add Another</button>
      					<br><br>
      					<button type="button" id="close-submit" class="btn btn-default pull-right" data-dismiss="modal">Submit</button>
    				</div>
			</div>
		</div>
	</div>
</div>
  </div>
  
    <!-- Login -->
  <div class="modal fade" id="Login" role="dialog">
  	<div class="modal-dialog">
  	<div class="modal-content" id="modal">
     <div class="omb_login">
    	<h3 class="omb_authTitle">Login or Sign up</h3>
		<div class="row omb_row-sm-offset-3 omb_socialButtons">
    	    <div class="col-xs-4 col-sm-2">
		        <a href="#" class="btn btn-lg btn-block omb_btn-facebook">
			        <i class="fa fa-facebook visible-xs"></i>
			        <span class="hidden-xs">Facebook</span>
		        </a>
	        </div>
        	<div class="col-xs-4 col-sm-2">
		        <a href="#" class="btn btn-lg btn-block omb_btn-twitter">
			        <i class="fa fa-twitter visible-xs"></i>
			        <span class="hidden-xs">Twitter</span>
		        </a>
	        </div>	
        	<div class="col-xs-4 col-sm-2">
		        <a href="#" class="btn btn-lg btn-block omb_btn-google">
			        <i class="fa fa-google-plus visible-xs"></i>
			        <span class="hidden-xs">Google+</span>
		        </a>
	        </div>	
		</div>
	    <h3 class="omb_authTitle"> Or </h3>
		<div class="row omb_row-sm-offset-3">
			<div class="col-xs-12 col-sm-6">	
			    <form class="omb_loginForm" action="" autocomplete="off" method="POST">
			     	<div class="input-group">
						<span class="input-group-addon"><i class="fa fa-user"></i></span>
						<input type="text" class="form-control" name="username" placeholder="UserName">
					</div>
					<span class="help-block"></span>
					<div class="input-group">
						<span class="input-group-addon"><i class="fa fa-user"></i></span>
						<input type="text" class="form-control" name="Email id" placeholder="email address">
					</div>
					<span class="help-block"></span>
										
					<div class="input-group">
						<span class="input-group-addon"><i class="fa fa-lock"></i></span>
						<input  type="password" class="form-control" name="password" placeholder="Password">
					</div>
                    <span class="help-block">Password error</span>

					<button class="btn btn-lg btn-primary btn-block" type="submit">Login</button>
				</form>
			</div>
    	</div>
		<div class="row omb_row-sm-offset-3">
			<div class="col-xs-12 col-sm-3">
				<label class="checkbox">
					<input type="checkbox" value="remember-me">Remember Me
				</label>
			</div>
			<div class="col-xs-12 col-sm-3">
				<p class="omb_forgotPwd">
					<a href="#">Forgot password?</a>
				</p>
			</div>
		</div>	    	
	</div>

</div>
        </div>
</div>
  
<script type="text/javascript">
function loadPage(id)
{
	switch(id)
	{
	case 'Main':
		window.location.href = "./Mainpage.php";
		break;
	case 'GifCollections':
		window.location.href = "./GifCollections.php";
		break;
	case 'MemeCollections':
		window.location.href = "./MemeCollections.php";
		break;
	case 'DevEnv':
		window.location.href = "./DevEnv.php";
		break;
	default:
		window.location.href = "./Mainpage.php";;
	}
	return false;
}
</script>

	