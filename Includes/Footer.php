	<div id ='footer'>
		<div class="container">
			<div class="btn-group btn-group-justified">
  				<div class="btn-group">
    				<button type="button" class="btn btn-link" data-toggle="modal" data-target="#About"> About </button>
				</div>
				<div class="btn-group">
    				<button type="button"  class="btn btn-link" data-toggle="modal" data-target="#Features"> Features </button>
				</div>
				<div class="btn-group">
					<button type="button"  class="btn btn-link" data-toggle="modal" data-target="#Issues"> Issues </button>
				</div>
				<div class="btn-group">
					<button type="button"  class="btn btn-link" data-toggle="modal" data-target="#Contact"> Contact </button>
				</div>
				<!-- if signed in as admin then show this -->
				<div class="btn-group">
    				<input type="button" class="btn btn-link" value="DevEnv"  onclick="return loadPage('DevEnv')">
				</div>
				<!--  till here -->
			</div>
		</div>
	</div>
	
	<!-- About Us -->
<div class="modal fade" id="About" role="dialog">
	<div class="modal-dialog">
		<div class="modal-content" id="modal">
				<h4 class="modal-title">About Us :</h4>
			<div class="modal-body">
				<p>A vampire is a being from folklore who subsists by feeding on the life essence (generally in the form of blood) of living creatures. Undead beings, vampires often visited loved ones and caused mischief or deaths in the neighbourhoods they inhabited when they were alive. They wore shrouds and were often described as bloated and of ruddy or dark countenance, markedly different from today's gaunt, pale vampire which dates from the early 19th century.

Although vampiric entities have been recorded in most cultures, the term vampire was not popularized in the west until the early 18th century, after an influx of vampire superstition into Western Europe from areas where vampire legends were frequent, such as the Balkans and Eastern Europe,[5] although local variants were also known by different names, such as shtriga in Albania, vrykolakas in Greece and strigoi in Romania. This increased level of vampire superstition in Europe led to what can only be called mass hysteria and in some cases resulted in corpses actually being staked and people being accused of vampirism.

In modern times, however, the vampire is generally held to be a fictitious entity, although belief in similar vampiric creatures such as the chupacabra still persists in some cultures. Early folk belief in vampires has sometimes been ascribed to the ignorance of the body's process of decomposition after death and how people in pre-industrial societies tried to rationalise this, creating the figure of the vampire to explain the mysteries of death. Porphyria was also linked with legends of vampirism in 1985 and received much media exposure, but has since been largely discredited.

The charismatic and sophisticated vampire of modern fiction was born in 1819 with the publication of The Vampyre by John Polidori; the story was highly successful and arguably the most influential vampire work of the early 19th century.[6] However, it is Bram Stoker's 1897 novel Dracula which is remembered as the quintessential vampire novel and provided the basis of the modern vampire legend. The success of this book spawned a distinctive vampire genre, still popular in the 21st century, with books, films, and television shows. The vampire has since become a dominant figure in the horror genre</p>
			</div>
				<button type="button" id="close-submit" class="btn btn-default pull-right" data-dismiss="modal"> Close</button>
		</div>
	</div>
</div>
  
  	<!-- Features -->
<div class="modal fade" id="Features" role="dialog">
	<div class="modal-dialog">
		<div class="modal-content" id="modal">
			<h4 class="modal-title">Features : </h4>
			<div class="modal-body" >
			    <h5>Features Requested</h5>
				<ul class="list-group">
 					<li class="list-group-item">Meme Generator</li>
  					<li class="list-group-item">Gif Generator</li>
					<li class="list-group-item">Marquee tool</li>
					<li class="list-group-item">Video tool</li>
					<li class="list-group-item">Followup posts</li>
				</ul>
				<h5>New Feature Request</h5>
    				<div class="form-row">
      					<div id="comment-message" class="form-row">
						<textarea name = "comment" placeholder = "Message" id = "comment" ></textarea>

      					<br>
      					<button type="button" id="close-submit" class="btn btn-default pull-right" data-dismiss="modal">Submit</button>
    				</div>
			</div>
		</div>
	</div>
</div>
  </div>
  
  <!-- Issues -->
  <div class="modal fade" id="Issues" role="dialog">
	<div class="modal-dialog">
		<div class="modal-content" id="modal">
			<h4 class="modal-title">Issues : </h4>
			<div class="modal-body" >
			    <h5>Known Issues</h5>
				<ul class="list-group" id="features">
 					<li class="list-group-item">Known Issues 1</li>
  					<li class="list-group-item">Known Issues 2</li>
					<li class="list-group-item">Known Issues 3</li>
					<li class="list-group-item">Known Issues 4</li>
					<li class="list-group-item">Known Issues 5</li>
				</ul>
				<h5>Report Issue:</h5>
				<form role="form">
    				<div class="form-group">
      					<label for="comment">Details:</label>
      					<textarea class="form-control" rows="5" id="comment"></textarea>
      					<button type="button" id="close-submit" class="btn btn-default pull-right" data-dismiss="modal">Submit</button>
    				</div>
  				</form>
			</div>
		</div>
	</div>
  </div>
  
  <!-- contact-->
<div class="modal fade" id="Contact" role="dialog">
	<div class="modal-dialog">
		<div class="modal-content" id="modal">
		<h4 class="modal-title">Contact Us : </h4>
			<div class="modal-body" >
			    <h5>Known Issues</h5>
			<form class="form-horizontal" role="form" method="post" action="index.php">
				<div class="form-group">
					<label for="name" class="col-sm-2 control-label">Name</label>
					<div class="col-sm-10">
						<input type="text" class="form-control" id="contactText" name="name" placeholder="First & Last Name" value="">
					</div>
					<label for="email" class="col-sm-2 control-label">Email</label>
					<div class="col-sm-10">
						<input type="email" class="form-control" id="contactText" name="email" placeholder="example@domain.com" value="">
					</div>
					<label for="message" class="col-sm-2 control-label">Message</label>
					<div class="col-sm-10">
						<textarea class="form-control" id="contactText" rows="4" name="message"></textarea>
					</div>
					<label for="human" class="col-sm-2 control-label">2 + 3 = ?</label>
					<div class="col-sm-10">
						<input type="text" class="form-control" id="contactText" name="human" placeholder="Your Answer">
					</div>
					<div class="col-sm-10 col-sm-offset-2">
						<button type="button" id="close-submit" class="btn btn-default pull-right" data-dismiss="modal">Submit</button>
					</div>
				</div>
			</form>
		</div>
	</div>
</div>
</div>
  
  
</body>
